(function($) {
	'use strict';
	
	// START MENU JS
	$(window).on('scroll', function() {
		if ($(this).scrollTop() > 50) {
			$('.main-navbar').addClass('menu-shrink');
		} else {
			$('.main-navbar').removeClass('menu-shrink');
		}
	});	
	
	$('.navbar-nav li a').on('click', function(e){
		var anchor = $(this);
		$('html, body').stop().animate({
			scrollTop: $(anchor.attr('href')).offset().top - 50
		}, 1000);
		e.preventDefault();
	});

	$(document).on('click','.navbar-collapse.show',function(e) {
		if( $(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle' ) {
			$(this).collapse('hide');
		}
	});	

    // Popup Video
	$('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
		disableOn: 300,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: false
	});

	// Clientes slider
	$('.clients-slider').owlCarousel({
		items: 1,
		loop: true,
		margin: 0,
		nav: true,
		autoplay: true,
		autoplayHoverPause: true,
		dots: false,
		navText: [
			"<i class='las la-long-arrow-alt-left'></i>",
			"<i class='las la-long-arrow-alt-right'></i>"
		]
	})
	
	// Partner Slider
	$('.partner-slider').owlCarousel({
		loop: true,
		dots: false,
		margin: 40,
		nav: false,
		autoplay: true,
		autoplayHoverPause: true,
		responsive:{
			0:{
				items:2
			},
			576:{
				items:3
			},
			768:{
				items:4
			},
			1200:{
				items:5
			}
		}
	})

	// Testimonials slider JS
	$('.testimonials-slider').owlCarousel({
		items: 1,
		loop: true,
		margin: 0,
		nav: true,
		autoplay: true,
		autoplayHoverPause: true,
		dots: false,
		navText: [
			"<i class='las la-long-arrow-alt-left'></i>",
			"<i class='las la-long-arrow-alt-right'></i>"
		]
	})

	// Clientes slider Two
	$('.clients-slider-two').owlCarousel({
		loop: true,
		margin: 20,
		nav: false,
		// autoplay: true,
		// autoplayHoverPause: true,
		dots: true,
		responsive:{
			0:{
				items:1
			},
			576:{
				items:1
			},
			768:{
				items:1
			},
			1200:{
				items:2
			}
		}
	})

	// Screens slider
	$('.screens-slider').owlCarousel({
		loop: true,
		margin: 10,
		nav: false,
		autoplay: true,
		autoplayHoverPause: true,
		dots: true,
		responsive:{
			0:{
				items:1
			},
			576:{
				items:2
			},
			768:{
				items:3
			},
			1200:{
				items:5
			}
		}
	})
	
	// Odometer JS
	$('.odometer').appear(function(e) {
		var odo = $(".odometer");
		odo.each(function() {
			var countNumber = $(this).attr("data-count");
			$(this).html(countNumber);
		});
	});

	// Tabs
	$('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');
	$('.tab ul.tabs li').on('click', function (g) {
		var tab = $(this).closest('.tab'), 
		index = $(this).closest('li').index();
		tab.find('ul.tabs > li').removeClass('current');
		$(this).closest('li').addClass('current');
		tab.find('.tab-content').find('div.tabs-item').not('div.tabs-item:eq(' + index + ')').slideUp();
		tab.find('.tab-content').find('div.tabs-item:eq(' + index + ')').slideDown();
		g.preventDefault();
	});

	
	// WOW JS
	new WOW().init();

	// Subscribe form JS
	$(".newsletter-form").validator().on("submit", function (event) {
		if (event.isDefaultPrevented()) {
			// handle the invalid form...
			formErrorSub();
			submitMSGSub(false, "Please enter your email correctly.");
		} else {
			// everything looks good!
			event.preventDefault();
		}
	});
	function callbackFunction (resp) {
		if (resp.result === "success") {
			formSuccessSub();
		}
		else {
			formErrorSub();
		}
	}
	function formSuccessSub(){
		$(".newsletter-form")[0].reset();
		submitMSGSub(true, "Thank you for subscribing!");
		setTimeout(function() {
			$("#validator-newsletter").addClass('hide');
		}, 4000)
	}
	function formErrorSub(){
		$(".newsletter-form").addClass("animated shake");
		setTimeout(function() {
			$(".newsletter-form").removeClass("animated shake");
		}, 1000)
	}
	function submitMSGSub(valid, msg){
		if(valid){
			var msgClasses = "validation-success";
		} else {
			var msgClasses = "validation-danger";
		}
		$("#validator-newsletter").removeClass().addClass(msgClasses).text(msg);
	}
	
	// AJAX MailChimp JS
	$(".newsletter-form").ajaxChimp({
		url: "https://EnvyTheme.us20.list-manage.com/subscribe/post?u=60e1ffe2e8a68ce1204cd39a5&amp;id=42d6d188d9", // Your url MailChimp
		callback: callbackFunction
	});

	// Preloader JS
	jQuery(window).on('load',function(){
		jQuery(".preloader").fadeOut(500);
	});

	// Go to Top
	$(window).on('scroll', function() {
        if ($(this).scrollTop() > 0) {
            $('.go-top').addClass('active');
        } else {
            $('.go-top').removeClass('active');
        }
	});	
    $(function(){
        $(window).on('scroll', function(){
            var scrolled = $(window).scrollTop();
            if (scrolled > 600) $('.go-top').addClass('active');
            if (scrolled < 600) $('.go-top').removeClass('active');
        });  
        
        $('.go-top').on('click', function() {
            $("html, body").animate({ scrollTop: "0" },  500);
        });
    });

})(jQuery);

// Login 
$(document).ready(function () {
	$('#logForm').submit(function (e) {
		e.preventDefault();
		$('#logText').html('<i class="mdi mdi-loading mdi-spin"></i>&nbsp; Checking...');
		var user = $('#logForm').serialize();
		var login = function () {
			$.ajax({
				type: 'POST',
				url: url + '/login/do_login',
				dataType: 'json',
				data: user,
				success: function (response) {
					$('#message').html(response.message);
					$('#logText').html('Login');
					if (response.error) {
						$('#responseDiv').removeClass('alert alert-success').addClass('alert alert-danger').show();
						$('#email').addClass("is-invalid");
						$('#password').addClass("is-invalid");
					} else {
						$('#responseDiv').removeClass('alert alert-danger').addClass('alert alert-success').show();
						// $('#logForm')[0].reset();
						$('#email').removeClass("is-invalid");
						$('#password').removeClass("is-invalid");
						$('#email').addClass("is-valid");
						$('#password').addClass("is-valid");
						$('.toggle-password').hide();
						setTimeout(function () {
							location.reload();
						}, 3000);
					}
				}
			});
		};
		setTimeout(login, 3000);
	});
})

// validasi hanya angka
function hanyaAngka(evt) {
	var theEvent = evt || window.event;

	// Handle paste
	if (theEvent.type === 'paste') {
		key = event.clipboardData.getData('text/plain');
	} else {
		// Handle key press
		var key = theEvent.keyCode || theEvent.which;
		key = String.fromCharCode(key);
	}
	var regex = /[0-9]|\./;
	if (!regex.test(key)) {
		theEvent.returnValue = false;
		if (theEvent.preventDefault) theEvent.preventDefault();
	}
}

// konvert ke rupiah
function convertToRupiah(angka) {
	var rupiah = '';
	var angkarev = angka.toString().split('').reverse().join('');
	for (var i = 0; i < angkarev.length; i++)
		if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
	return 'Rp. ' + rupiah.split('', rupiah.length - 1).reverse().join('');
}

// wakili kedatangan
$("#btnWakili").on("click", function(){
	const boxDatang = $("#boxDatang");
	const boxWakili = $("#boxWakili");
	boxDatang.addClass("d-none");
	boxWakili.toggleClass("d-none");
	boxWakili.find("#namaWakili").focus();
})

// $('#namaWakili').autocomplete({
// 	source: "<?php echo site_url('registrasi/get_autocomplete');?>",

// 	select: function (event, ui) {
// 		$('[name="nama"]').val(ui.item.nama); 
// 		$('[name="no_telp"]').val(ui.item.no_telp); 
// 	}
// });

$( "#namaWakili" ).autocomplete({
	source: function( request, response ) {
		// Fetch data
		$.ajax({
		url: url+"registrasi/get_autocomplete",
		type: 'post',
		dataType: "json",
		data: {
			search: request.term
		},
		success: function( data ) {
			response( data );
		}
		});
	},
	select: function (event, ui) {
		// Set selection
		$('#namaWakili').val(ui.item.nama); // display the selected text
		$('#telpWakili').val(ui.item.no_telp); // save selected id to input
		return false;
	}
});

$('#datang').on("click", function (e) {
	e.preventDefault();
	const link = $(this).attr("href");
	const boxDatang = $("#boxDatang");
	const boxWakili = $("#boxWakili");
	boxDatang.toggleClass("d-none");
	boxWakili.addClass("d-none");
	boxDatang.find("#namaTamu").focus();
});

$('#formDatang').submit(function (e) {
	e.preventDefault();
	var data = $('#formDatang').serialize();
	$.ajax({
		type: 'POST',
		url: url + 'datang',
		dataType: 'json',
		data: data,
		beforeSend: function(){
			$('#namaTamu').addClass("disabled");
			$('#alamatTamu').addClass("disabled");
			$('#emailTamu').addClass("disabled");
			$('#instansiTamu').addClass("disabled");
			$('#jabatanTamu').addClass("disabled");
			$('#simpanDatang').html('<i class="las la-spinner la-spin"></i>&nbsp; Mengirim Data...');
		},
		success: function (response) {
			$('#message').html(response.message);
			$('#simpanDatang').html('Verifikasi');
			if (response.error) {
				$('#responseDiv').removeClass('alert alert-success').addClass('alert alert-danger').show();
				$('#namaTamu').addClass("is-invalid");
				$('#alamatTamu').addClass("is-invalid");
				$('#emailTamu').addClass("is-invalid");
				$('#instansiTamu').addClass("is-invalid");
				$('#jabatanTamu').addClass("is-invalid");
				$('#namaTamu').removeClass("disabled");
				$('#alamatTamu').removeClass("disabled");
				$('#emailTamu').removeClass("disabled");
				$('#instansiTamu').removeClass("disabled");
				$('#jabatanTamu').removeClass("disabled");
			} else {
				$('#responseDiv').removeClass('alert alert-danger').addClass('alert alert-success').show();
				$("#formDatang").hide();
				setTimeout(function () {
					location.reload();
				}, 3000);
			}
		}
	});
});

$('#formWakili').submit(function (e) {
	e.preventDefault();
	var data = $('#formWakili').serialize();
	$.ajax({
		type: 'POST',
		url: url + 'simpan_wakili',
		dataType: 'json',
		data: data,
		beforeSend: function(){
			$('#namaWakili').addClass("disabled");
			$('#telpWakili').addClass("disabled");
			$('#simpanWakili').html('<i class="las la-spinner la-spin"></i>&nbsp; Mengirim Data...');
		},
		success: function (response) {
			$('#messageWakili').html(response.message);
			$('#simpanWakili').html('Simpan');
			if (response.error) {
				$('#responseDivWakili').removeClass('alert alert-success').addClass('alert alert-danger').show();
				$('#namaWakili').addClass("is-invalid");
				$('#telpWakili').addClass("is-invalid");
				$('#namaWakili').removeClass("disabled");
				$('#telpWakili').removeClass("disabled");
			} else {
				$('#responseDivWakili').removeClass('alert alert-danger').addClass('alert alert-success').show();
				$("#formWakili").hide();
				setTimeout(function () {
					location.reload();
				}, 3000);
			}
		}
	});
});

$('#datangWakili').on("click", function (e) {
	e.preventDefault();
	const link = $(this).attr("href");
	const boxDatang = $("#boxDatangWakili");
	boxDatang.toggleClass("d-none");
	boxDatang.find("#namaTamu").focus();
});

$('#formDatangWakili').submit(function (e) {
	e.preventDefault();
	var data = $('#formDatangWakili').serialize();
	$.ajax({
		type: 'POST',
		url: url + 'datang_wakili',
		dataType: 'json',
		data: data,
		beforeSend: function(){
			$('#namaTamu').addClass("disabled");
			$('#alamatTamu').addClass("disabled");
			$('#emailTamu').addClass("disabled");
			$('#instansiTamu').addClass("disabled");
			$('#jabatanTamu').addClass("disabled");
			$('#simpanDatangWakili').html('<i class="las la-spinner la-spin"></i>&nbsp; Mengirim Data...');
		},
		success: function (response) {
			$('#messageDatangWakili').html(response.message);
			$('#simpanDatangWakili').html('Verifikasi');
			if (response.error) {
				$('#responseDivDatangWakili').removeClass('alert alert-success').addClass('alert alert-danger').show();
				$('#namaTamu').addClass("is-invalid");
				$('#alamatTamu').addClass("is-invalid");
				$('#emailTamu').addClass("is-invalid");
				$('#instansiTamu').addClass("is-invalid");
				$('#jabatanTamu').addClass("is-invalid");
				$('#namaTamu').removeClass("disabled");
				$('#alamatTamu').removeClass("disabled");
				$('#emailTamu').removeClass("disabled");
				$('#instansiTamu').removeClass("disabled");
				$('#jabatanTamu').removeClass("disabled");
			} else {
				$('#responseDivDatangWakili').removeClass('alert alert-danger').addClass('alert alert-success').show();
				$("#formDatangWakili").hide();
				setTimeout(function () {
					location.reload();
				}, 3000);
			}
		}
	});
});
