<?php

class Whatsapp
{
    private $token;

    public function __construct()
    {
        $this->CI = &get_instance();
        require_once APPPATH . '/third_party/guzzle/autoload.php';
        $this->token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Imdvcm9udGFsb2thYiIsInJvbGUiOiJhZG1pbiIsImlhdCI6MTYxNTc4NDI0MH0.8QHok2pCr1R9abm-DualP_F3cJs3FBjHrww8Vkzs7PU';
    }

    public function sendMessage($number = null, $message = null)
    {
        try {
            $client = new \GuzzleHttp\Client(["base_uri" => "http://103.94.3.39:443"]);
            $headers = [
                'Authorization' => 'Bearer ' . $this->token,
                'Accept' => 'application/json',
            ];

            $response = $client->post("/send-message", [
                'headers' => $headers,
                'form_params' => [
                    'number' => $number,
                    'message' => $message,
                ],
            ]);
            return $response->getBody();
        } catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            return $response->getBody()->getContents();
        }
    }

    public function sendImage($number = null, $caption = null, $media)
    {
        try {
            $client = new \GuzzleHttp\Client(["base_uri" => "http://103.94.3.39:443"]);
            $headers = [
                'Authorization' => 'Bearer ' . $this->token,
                'Accept' => 'application/json',
            ];

            $response = $client->post("/send-image", [
                'headers' => $headers,
                'form_params' => [
                    'number' => $number,
                    'caption' => $caption,
                    'file' => $media,
                ],
            ]);
            return $response->getBody();
        } catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();

            return $response->getBody()->getContents();
        }
    }

    public function sendDocument($number = null, $caption = null, $media)
    {
        try {
            $client = new \GuzzleHttp\Client(["base_uri" => "http://103.94.3.39:443"]);
            $headers = [
                'Authorization' => 'Bearer ' . $this->token,
                'Accept' => 'application/json',
            ];

            $response = $client->post("/send-document", [
                'headers' => $headers,
                'form_params' => [
                    'number' => $number,
                    'caption' => $caption,
                    'file' => $media,
                ],
            ]);
            return $response->getBody();
        } catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();

            return $response->getBody()->getContents();
        }
    }

    public function checkNumber($number = null)
    {
        $client = new \GuzzleHttp\Client(["base_uri" => "http://103.94.3.39:443"]);
        $headers = [
            'Authorization' => 'Bearer ' . $this->token,
            'Accept' => 'application/json',
        ];

        try {

            $response = $client->post("/check-number", [
                'headers' => $headers,
                'form_params' => [
                    'number' => $number,
                ],
            ]);
            return $response->getBody();
        } catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();

            return $response->getBody()->getContents();
        }
    }

    public function checkMessageByID($number = null, $chatid = null)
    {
        $client = new \GuzzleHttp\Client(["base_uri" => "http://103.94.3.39:443"]);

        try {

            $response = $client->post("/check-messagebyid", [
                'form_params' => [
                    'number' => $number,
                    'chatid' => $chatid,
                ],
            ]);
            return $response->getBody();
        } catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();

            return $response->getBody()->getContents();
        }
    }
}
