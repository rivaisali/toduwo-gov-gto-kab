<?php $this->load->view('frontend/inc/head_html'); ?>
<div id="features" class="our-services-area service-shape pt-100 pb-70">
    <div class="container text-center">
        <div class="section-title">
            <h1>404</h1>
            <p>Halaman tidak ditemukan</p>
        </div>
        <a href="<?= base_url(); ?>" class="btn btn-primary btn-lg mx-auto text-center"><i class="la la-home"></i> Kembali</a>
    </div>
</div>
<?php $this->load->view('frontend/inc/foot_html'); ?>