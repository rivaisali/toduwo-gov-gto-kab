<div class="row">
    <div class="col-12">
        <div class="card card-border-color card-border-color-primary">
            <!-- <div class="card-header card-header-divider"><?= $title; ?><span class="card-subtitle"></span></div> -->
            <div class="card-body">
                <?= form_open("", ["autocomplete" => "off"]); ?>
                <?= form_hidden('id', $data->acara_id); ?>
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="form-group pt-2">
                            <label for="nama_acara">Nama Acara <i class="text-danger">*</i></label>
                            <input class="form-control form-control-sm <?= form_error('nama_acara') ? 'is-invalid' : ''; ?>" name="nama_acara" id="nama_acara" type="text" placeholder="Resepsi / Akad Nikah" value="<?= set_value('nama_acara', $data->nama_acara); ?>" autofocus>
                            <?= form_error('nama_acara'); ?>
                        </div>
                        <div class="form-group pt-2">
                            <label for="tgl">Tanggal dan Jam <i class="text-danger">*</i></label>
                            <div class="input-group date datetimepicker" data-start-view="0" data-date="" data-date-format="yyyy-mm-dd - HH:ii" data-link-field="dtp_input1">
                                <div class="input-group-append input-group-sm">
                                    <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                                </div>
                                <input name="tgl" id="tgl" class="form-control form-control-sm <?= form_error('tgl') ? 'is-invalid' : ''; ?>" size="16" type="text" value="<?= set_value('tgl', $data->tanggal . " " . $data->jam); ?>">
                                <?= form_error('tgl'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group pt-2">
                            <label for="lokasi">Lokasi <i class="text-danger">*</i></label>
                            <input class="form-control form-control-sm <?= form_error('lokasi') ? 'is-invalid' : ''; ?>" id="lokasi" type="text" placeholder="Lokasi Acara" name="lokasi" value="<?= set_value('lokasi', $data->nama_lokasi); ?>">
                            <?= form_error('lokasi'); ?>
                        </div>

                        <div class="form-group pt-2">
                            <label for="alamat">Alamat Lokasi <i class="text-danger">*</i></label>
                            <input class="form-control form-control-sm <?= form_error('alamat') ? 'is-invalid' : ''; ?>" id="alamat" type="text" placeholder="Alamat Lokasi" name="alamat" value="<?= set_value('alamat', $data->alamat_lokasi); ?>">
                            <?= form_error('lokasi'); ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <button class="btn btn-space btn-primary" type="submit">Simpan</button>
                        <a href="<?= base_url("pengaturan/acara"); ?>" class="btn btn-space btn-secondary">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>