<div class="row">
    <div class="col-lg-6">
        <div class="card card-border-color card-border-color-primary">
            <div class="card-body">
                <?= form_open("", ["autocomplete" => "off", "enctype" => "multipart/form-data"]); ?>
                <div class="form-group pt-1">
                    <label for="judul">Judul</label>
                    <input class="form-control form-control-sm <?= form_error('judul') ? 'is-invalid' : ''; ?>" name="judul" id="judul" type="text" placeholder="Judul" value="<?= set_value('judul', ($this->session->userdata("judul_broadcast") ? $this->session->userdata("judul_broadcast") : '')); ?>" autofocus>
                    <span class="text-muted mt-1 d-block">Judul tidak akan dimasukkan ke dalam pesan WA.</span>
                    <?= form_error('judul'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="isi_pesan">Isi Pesan</label>
                    <textarea name="isi_pesan" id="isi_pesan" class="form-control form-control-sm <?= form_error('isi_pesan') ? 'is-invalid' : ''; ?>" rows="8" placeholder="Masukkan pesan disini. Dengan mengukuti aturan penulisan"><?= set_value('isi_pesan', ($this->session->userdata("pesan_broadcast") ? $this->session->userdata("pesan_broadcast") : '')) ?></textarea>
                    <?= form_error('isi_pesan'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="gambar">Gambar</label>
                    <div class="row">
                        <div class="col-md-8">
                            <input type="file" name="gambar" class="form-control form-control-file form-control-sm <?= form_error('gambar') ? 'is-invalid' : ''; ?> mb-1" id="gambar" onchange="previewGambar()">
                            <?= form_error('gambar'); ?>
                            <input type="text" name="caption" class="my-2 form-control form-control-sm <?= form_error('caption') ? 'is-invalid' : ''; ?> mb-1" id="caption" value="<?= set_value('caption', ($this->session->userdata("caption_gambar") ? $this->session->userdata("caption_gambar") : '')); ?>" placeholder="Caption Gambar">
                            <?= form_error('caption'); ?>
                        </div>
                        <div class="col-md-4">
                            <?php
                            if ($this->session->userdata("gambar_broadcast")) {
                                $gambar =   $this->session->userdata("gambar_broadcast");
                            } else {
                                $gambar =   "default.jpg";
                            }
                            ?>
                            <img src="<?= base_url("uploads/broadcast/" . $gambar); ?>" class="img-thumbnail gambar-preview">
                        </div>
                    </div>
                </div>
                <div class="row pt-3">
                    <div class="col-12">
                        <p class="text-left">
                            <button class="btn btn-space btn-primary" type="submit"><i class="icon icon-left mdi mdi-accounts-add"></i>Lanjut Pilih Penerima</button>
                            <!-- <p class="text-left"> -->
                            <!-- <button class="btn btn-big btn-primary" type="submit"><i class="mdi mdi-accounts-add"></i> Lanjut Pilih Penerima</button> -->
                            <a href="<?= base_url($base); ?>" class="btn btn-space btn-secondary">Cancel</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card card-border-color card-border-color-danger">
            <div class="card-header card-header-divider">
                <div class="card-title">Aturan Penulisan Pesan</div>
            </div>
            <div class="card-body">
                <div class="aturan-pesan">
                    <code>\n</code> : Baris Baru<br>
                </div>
                <div class="aturan-pesan">
                    <code>*$pesan*</code> : <b>Tulisan Tebal</b><br>
                </div>
                <div class="aturan-pesan">
                    <code>_$pesan_</code> : <i>Tulisan Miring</i><br>
                </div>
                <div class="aturan-pesan">
                    <code>~$pesan~</code> : <s>Tulisan Bergaris</s><br>
                </div>
            </div>
        </div>
    </div>
</div>