<div class="row">
    <div class="col-lg-6">
        <div class="card card-border-color card-border-color-primary">
            <div class="card-body">
                <?= form_open("", ["autocomplete" => "off"]); ?>
                <div class="form-group pt-1">
                    <label for="nama">Nama Grup</label>
                    <input class="form-control form-control-sm <?= form_error('nama') ? 'is-invalid' : ''; ?>" name="nama" id="nama" type="text" placeholder="Nama Grup" value="<?= set_value('nama', ''); ?>">
                    <?= form_error('nama'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="deskripsi">Deskripsi</label>
                    <textarea name="deskripsi" id="deskripsi" rows="3" class="form-control form-control-sm <?= form_error('deskripsi') ? 'is-invalid' : ''; ?>"><?= set_value('Deskripsi', ''); ?></textarea>
                    <?= form_error('deskripsi'); ?>
                </div>
                <div class="row pt-3">
                    <div class="col-sm-6">
                        <p class="text-left">
                            <button class="btn btn-space btn-primary" type="submit">Simpan</button>
                            <a href="<?= base_url($base); ?>" class="btn btn-space btn-secondary">Cancel</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>