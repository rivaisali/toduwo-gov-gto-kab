<div class="row">
    <div class="col-lg-6">
        <div class="card card-border-color card-border-color-primary">
            <!-- <div class="card-header card-header-divider"><?= $title; ?><span class="card-subtitle"></span></div> -->
            <div class="card-body">
                <?= form_open("", ["autocomplete" => "off"]); ?>
                <input type="hidden" name="undangan_id" id="undangan_id" value="<?= $undangan->undangan_id; ?>">
                <div class="form-group pt-1">
                    <label for="no_telp_cari">Telp / WA</label>
                    <input class="form-control form-control-sm <?= form_error('no_telp') ? 'is-invalid' : ''; ?>" name="no_telp" id="no_telp_cari" type="text" placeholder="Masukkan No Telp" value="<?= set_value('no_telp', ''); ?>" autofocus>
                    <?= form_error('no_telp'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="nama">Nama</label>
                    <input class="form-control form-control-sm <?= form_error('nama') ? 'is-invalid' : ''; ?>" name="nama" id="nama" type="text" placeholder="Nama" value="<?= set_value('nama', ''); ?>">
                    <?= form_error('nama'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="nama">Alamat</label>
                    <textarea name="alamat" id="alamat" rows="3" class="form-control form-control-sm <?= form_error('alamat') ? 'is-invalid' : ''; ?>"><?= set_value('alamat', ''); ?></textarea>
                    <?= form_error('alamat'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="email">Email</label>
                    <input class="form-control form-control-sm <?= form_error('email') ? 'is-invalid' : ''; ?>" name="email" id="email" type="text" placeholder="nama@example.com" value="<?= set_value('email', ''); ?>">
                    <?= form_error('email'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="instansi">Instansi / Organisasi</label>
                    <input class="form-control form-control-sm <?= form_error('instansi') ? 'is-invalid' : ''; ?>" name="instansi" id="instansi" type="text" placeholder="Instansi / Organisasi" value="<?= set_value('instansi', ''); ?>">
                    <?= form_error('instansi'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="jabatan">Jabatan</label>
                    <input class="form-control form-control-sm <?= form_error('jabatan') ? 'is-invalid' : ''; ?>" name="jabatan" id="jabatan" type="text" placeholder="Jabatan" value="<?= set_value('jabatan', ''); ?>">
                    <?= form_error('jabatan'); ?>
                </div>
                <div class="row pt-3">
                    <div class="col-sm-6">
                        <p class="text-left">
                            <button class="btn btn-space btn-primary" type="submit">Simpan</button>
                            <a href="<?= base_url($base); ?>" class="btn btn-space btn-secondary">Cancel</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>