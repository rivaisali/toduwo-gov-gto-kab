<div class="row">
    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">Daftar <?= $title; ?>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Judul</th>
                            <th>Waktu</th>
                            <th>Lokasi</th>
                            <th>Tamu</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($data as $d) : ?>
                            <tr class="odd gradeX">
                                <td><?= $no++; ?></td>
                                <td><?= $d->judul; ?></td>
                                <td>
                                    <?php if ($d->tanggal == $d->tanggal_selesai) {
                                        echo tgl_full($d->tanggal . " " . $d->jam) . " Sampai " . jam($d->tanggal_selesai . " " . $d->jam_selesai);
                                    } else {
                                        echo tgl_full($d->tanggal . " " . $d->jam) . " <br>s/d<br> " . tgl_full($d->tanggal_selesai . " " . $d->jam_selesai);
                                    }
                                    ?>
                                </td>
                                <td><?= $d->nama_lokasi; ?></td>
                                <td>
                                    <?= ambil_numrow_by_id("tamu_undangan", "undangan_id", $d->undangan_id); ?>
                                </td>
                                <td class="actions">
                                    <a title="Detail" class="icon mx-1" href="<?= base_url("detail_absen/" . $d->undangan_id); ?>">
                                        <button class="btn btn-warning"> <i class="mdi mdi-assignment-check text-white"></i> Daftar Hadir</button>
                                    </a>
                                    <a title="Detail" class="icon mx-1" href="<?= base_url("detail_riwayat/" . $d->undangan_id); ?>">
                                        <button class="btn btn-primary"><i class="mdi mdi-view-list text-white"></i> Detil</button>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>