<?php $this->load->view('frontend/inc/head_html'); ?>
<div id="page-content">
	<div class="text-center my-3">
		<!-- <h1 class="text-primary">SIPARDI</h1> -->
		<img src="<?= base_url("assets/img/top-logo.png"); ?>" alt="Logo SIPARDI" class="" width="200">
	</div>
	<div class="container mb-2">
		<div class="alert alert-success" role="alert">
			<h4 class="alert-heading">Selamat!</h4>
			<p>Akun anda telah diverifikasi. Silahkan masuk menggunakan <b>Email</b> dan <b>Password</b> yang telah anda masukkan pada saat registrasi.</p>
			<hr>
			<p class="mb-0"><a href="<?= base_url("login"); ?>">Klik Disini</a> untuk masuk</p>
		</div>
	</div>
</div>
<?php $this->load->view('frontend/inc/footer'); ?>
<?php $this->load->view('frontend/inc/foot_html'); ?>