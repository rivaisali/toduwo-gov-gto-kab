   <!-- App Landing Banner Area -->
        <div id="home" class="app-banner-area pt-100">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="app-bennr-text">
                            <span>Pemerintah Kabupaten Gorontalo</span>
                            <h3>Undangan Digital</h3>
                            <p>Platform Undangan Digital Terlengkap untuk Solusi Aman mengirim undangan saat pandemi Covid-19.</p>
                            <div class="app-btn">
                                <a href="#LogInModal" class="default-btn app-btn-1"  data-toggle="modal" data-target="#LogInModal">Masuk Login</a>
                            </div>

                            <div class="row mt-30">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="app-text">
                                        <h3>100 &nbsp;</h3>
                                        <p>Undangan <br> Tersebar</p>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-sm-6">
                                    <div class="app-text">
                                        <h3>16.2k</h3>
                                        <p>Positive <br> Reviews</p>
                                    </div>
                                </div>
                            </div>

                            <div class="app-shapes">
                                <img src="<?=base_url();?>assets/images/shape/shape1.png" class="shape-1" alt="Image">
                                <img src="<?=base_url();?>assets/images/shape/shape30.png" class="shape-30"  alt="Image">
                                <img src="<?=base_url();?>assets/images/shape/shape28.png" class="shape-28"  alt="Image">
                                <img src="<?=base_url();?>assets/images/shape/shape16.png" class="shape-16"  alt="Image">
                                <img src="<?=base_url();?>assets/images/shape/shape16.png" class="shape-11"  alt="Image">
                                <img src="<?=base_url();?>assets/images/shape/shape14.png" class="shape-14"  alt="Image">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="app-banner-img">
                            <img src="<?=base_url();?>assets/images/banner.png" class="app-img" alt="Image">
                            <div class="app-shapes">
                                <img src="<?=base_url();?>assets/images/shape/shape31.png" class="shape-31"  alt="Image">
                                <img src="<?=base_url();?>assets/images/shape/shape4.png" class="shape-4"  alt="Image">
                                <img src="<?=base_url();?>assets/images/shape/shape2.png" class="shape-2"  alt="Image">
                                <img src="<?=base_url();?>assets/images/shape/shape22.png" class="shape-22"  alt="Image">
                                <img src="<?=base_url();?>assets/images/shape/shape5.png" class="shape-5"  alt="Image">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-shape">
                    <img src="<?=base_url();?>assets/images/shape/shape29.png" alt="Image">
                </div>
            </div>
        </div>

            <!-- Log In Modal -->
        <div class="modal custom-modal fade" id="LogInModal">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Silahkan Masuk </h5>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                      <center><img src="<?=base_url()?>assets/images/logo-toduwo.png" height="90"></center>
                         <div id="responseDiv">
							<div id="message">
                                <?=$this->session->userdata("user");?>
                            </div>
						</div>
                        <div class="user-form">
                            <form class="login-form" method="POST" id="logForm" autocomplete="on">
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group position-relative">
										<label>Email <span class="text-danger">*</span></label>

										<input type="email" class="form-control" placeholder="Email" name="email" id="email" required="" autofocus>
									</div>
								</div>

								<div class="col-lg-12">
									<div class="form-group position-relative">
										<label>Password <span class="text-danger">*</span></label>

										<input type="password" class="form-control" placeholder="Password" name="password" id="password" required="">
									</div>
								</div>


								<div class="col-lg-12 mb-0" align="right">
									<button type="submit" class="default-btn" id="logText">&nbsp;&nbsp;Masuk&nbsp;&nbsp;</button>
								</div>

							</div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>