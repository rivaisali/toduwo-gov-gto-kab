
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Toduwo - Website dan Undangan Pernikahan Digital</title>
    <script>
        const url = 'http://toduwo.id/';
    </script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
    <meta name="keywords" content="Saas, Software, multi-uses, HTML, Clean, Modern" />
    <meta name="author" content="Shreethemes" />
    <meta name="email" content="shreethemes@gmail.com" />
    <meta name="website" content="http://www.shreethemes.in" />
    <meta name="Version" content="v2.6" />
    <!-- favicon -->
    <link rel="shortcut icon" href="http://toduwo.id//assets/images/favicon.ico">
    <!-- Bootstrap -->
    <link href="http://toduwo.id//assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Magnific -->
    <link href="http://toduwo.id//assets/css/magnific-popup.css" rel="stylesheet" type="text/css" />
    <!-- Icons -->
    <link href="http://toduwo.id//assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v3.0.3/css/line.css">
    <!-- Slider -->
    <link rel="stylesheet" href="http://toduwo.id//assets/css/owl.carousel.min.css"/>
    <link rel="stylesheet" href="http://toduwo.id//assets/css/owl.theme.default.min.css"/>
    <!-- Main Css -->
    <link href="http://toduwo.id//assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
    <link href="http://toduwo.id//assets/css/colors/default.css" rel="stylesheet" id="color-opt">
<style>
    * {margin: 0; padding: 0}
    html {
        scroll-behavior: smooth;
    }
</style>
</head>

<body><!-- <div id="responseDiv">


<div class="back-to-home rounded d-none d-sm-block">
	<a href="http://toduwo.id/" class="btn btn-icon btn-soft-primary"><i data-feather="home" class="icons"></i></a>
</div>

<!-- Hero Start -->
<section class="bg-home d-flex align-items-center">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-7 col-md-6">
				<div class="mr-lg-5">
					<img src="http://toduwo.id/assets/images/user/login.svg" class="img-fluid d-block mx-auto" alt="">
				</div>
			</div>
			<div class="col-lg-5 col-md-6">
				<div class="card login-page bg-white shadow rounded border-0">
					<div class="card-body">

						<div id="responseDiv">
							<div id="message"></div>
						</div>

						<h4 class="card-title text-center">Login</h4>
												<form class="login-form mt-4" method="POST" id="logForm" autocomplete="on">
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group position-relative">
										<label>Your Email <span class="text-danger">*</span></label>

										<input type="email" class="form-control pl-5" placeholder="Email" name="email" id="email" required="" autofocus>
									</div>
								</div>

								<div class="col-lg-12">
									<div class="form-group position-relative">
										<label>Password <span class="text-danger">*</span></label>

										<input type="password" class="form-control pl-5" placeholder="Password" name="password" id="password" required="">
									</div>
								</div>
								</div>
								<div class="col-lg-12 mb-0">
									<button type="submit" class="btn btn-primary btn-block" id="logText">Masuk</button>
								</div>

							</div>
						</form>
					</div>
				</div>
				<!---->
			</div>
			<!--end col-->
		</div>
		<!--end row-->
	</div>
	<!--end container-->
</section>
<!--end section-->
<!-- Hero End -->



    <!-- Back to top -->
    <a href="#" class="btn btn-icon btn-primary back-to-top"><i data-feather="arrow-up" class="icons"></i></a>
    <!-- Back to top -->


    <!-- javascript -->
    <script src="http://toduwo.id/assets/js/jquery-3.5.1.min.js"></script>
    <script src="http://toduwo.id/assets/js/bootstrap.bundle.min.js"></script>
    <script src="http://toduwo.id/assets/js/jquery.easing.min.js"></script>
    <script src="http://toduwo.id/assets/js/scrollspy.min.js"></script>
 <script src="<?=base_url();?>assets/js/login.js"></script>



</body>
</html>
