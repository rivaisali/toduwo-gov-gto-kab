  <!-- Nabvar Area -->
        <nav class="navbar fixed-top navbar-expand-lg main-navbar app-nav" style="border:none;">
            <div class="container">
                <a class="navbar-brand" href="app-landing.html">
                    <img src="assets/images/logo-toduwo.png" height="90" alt="logo">
                </a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar top-bar"></span>
                    <span class="icon-bar middle-bar"></span>
                    <span class="icon-bar bottom-bar"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav m-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#steps">Steps</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#features">Features</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#screenshots">Screenshots</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#testimonials">Testimonials</a>
                        </li>
                    </ul>
                    <!-- <div class="nav-btn">
                        <a href="#" class="default-btn bg-main">Login</a>
                    </div> -->
                </div>
            </div>
        </nav>
        <!-- End Nabvar Area -->
