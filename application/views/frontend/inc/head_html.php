    <!doctype html>
    <html lang="zxx">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.min.css">
        <!-- Animate CSS -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/animate.min.css">
        <!-- Magnific CSS -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/magnific-popup.css">
        <!-- Owl Carousel CSS -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/owl.theme.default.min.css">
        <!-- Line Awesome CSS -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/line-awesome.min.css">
        <!-- Odometer CSS -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/odometer.css">
        <!-- Stylesheet CSS -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/style.css">
        <!-- Stylesheet Responsive CSS -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/responsive.css">
        <!-- jquery autocomplete -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.min.css">
        <!-- Favicon -->
        <!-- <link rel="icon" type="images/png" href="<?= base_url(); ?>assets/images/favicon.png"> -->
        <!-- Title -->
        <!-- <title>Toduwo.id</title> -->
        <link rel="shortcut icon" href="<?= base_url(); ?>assets_admin/img/logo-icon.png">
        <title>Toduwo.id</title>
        <script>
            const url = '<?php echo base_url(); ?>';
        </script>
    </head>

    <body data-spy="scroll" data-offset="70">