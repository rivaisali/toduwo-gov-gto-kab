  <!-- Go Top -->
  <div class="go-top">
    <i class="las la-angle-double-up"></i>
  </div>
  <!-- End Go Top -->

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
  <script src="<?= base_url(); ?>assets/js/popper.min.js"></script>
  <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
  <!-- jquery ui autocomplete -->
  <script src="<?= base_url(); ?>assets/js/jquery-ui.min.js"></script>
  <!-- Owl carousel JS -->
  <script src="<?= base_url(); ?>assets/js/owl.carousel.min.js"></script>
  <!-- Magnific JS -->
  <script src="<?= base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>
  <!-- Wow JS -->
  <script src="<?= base_url(); ?>assets/js/wow.min.js"></script>
  <!-- Odometer JS -->
  <script src="<?= base_url(); ?>assets/js/odometer.min.js"></script>
  <!-- Jquery Apper JS -->
  <script src="<?= base_url(); ?>assets/js/jquery.appear.js"></script>
  <!-- Form Validator JS -->
  <script src="<?= base_url(); ?>assets/js/form-validator.min.js"></script>
  <!-- Contact JS -->
  <script src="<?= base_url(); ?>assets/js/contact-form-script.js"></script>
  <!-- Ajaxchimp JS -->
  <script src="<?= base_url(); ?>assets/js/jquery.ajaxchimp.min.js"></script>
  <!-- Custom JS -->
  <script src="<?= base_url(); ?>assets/js/custom.js"></script>
  </body>

  </html>