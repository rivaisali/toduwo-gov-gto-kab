<!-- Footer Start -->
<footer class="footer py-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-12 mb-0">
                <a href="<?= base_url(); ?>/assets/#" class="logo-footer">
                    <img src="<?= base_url("assets/images/logo-dark.png"); ?>" height="24" alt="">
                </a>
                <p class="mt-4">Penyedia jasa pembuatan undangan online.</p>
                <!--end icon-->
            </div>
            <!--end col-->

            <div class="col-lg-2 col-md-4 col-12 mt-0">
                <ul class="list-unstyled social-icon social mb-0 mt-4">
                    <li class="list-inline-item"><a href="<?= base_url(); ?>/assets/javascript:void(0)" class="rounded"><i data-feather="facebook" class="fea icon-sm fea-social"></i></a></li>
                    <li class="list-inline-item"><a href="<?= base_url(); ?>/assets/javascript:void(0)" class="rounded"><i data-feather="instagram" class="fea icon-sm fea-social"></i></a></li>
                    <li class="list-inline-item"><a href="<?= base_url(); ?>/assets/javascript:void(0)" class="rounded"><i data-feather="twitter" class="fea icon-sm fea-social"></i></a></li>
                    <li class="list-inline-item"><a href="<?= base_url(); ?>/assets/javascript:void(0)" class="rounded"><i data-feather="linkedin" class="fea icon-sm fea-social"></i></a></li>
                </ul>
            </div>
            <!--end col-->

            <div class="col-lg-4 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <!-- <h4 class="text-light footer-head">Usefull Links</h4> -->
                <ul class="list-unstyled footer-list mt-4">
                    <li><i class="mdi mdi-chevron-right mr-1"></i> Kunjungan Hari Ini : <?= $kunjungan["hari_ini"]; ?></a></li>
                    <li><i class="mdi mdi-chevron-right mr-1"></i> Total Kunjungan : <?= $kunjungan["total"]; ?></a></li>
                </ul>
            </div>
            <!--end col
        </div>
        <!--end row-->
        </div>
        <!--end container-->
</footer>
<!--end footer-->
<footer class="footer footer-bar py-1">
    <div class="container text-center">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <div class="text-sm-left">
                    <p class="mb-0">© 2019-20 toduwo.id</p>
                </div>
            </div>
            <!--end col-->

            <div class="col-sm-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <ul class="list-unstyled text-sm-right mb-0">
                    <li class="list-inline-item"><a href="<?= base_url(); ?>/assets/javascript:void(0)"><img src="<?= base_url(); ?>images/payments/american-ex.png" class="avatar avatar-ex-sm" title="American Express" alt=""></a></li>
                    <li class="list-inline-item"><a href="<?= base_url(); ?>/assets/javascript:void(0)"><img src="<?= base_url(); ?>images/payments/discover.png" class="avatar avatar-ex-sm" title="Discover" alt=""></a></li>
                    <li class="list-inline-item"><a href="<?= base_url(); ?>/assets/javascript:void(0)"><img src="<?= base_url(); ?>images/payments/master-card.png" class="avatar avatar-ex-sm" title="Master Card" alt=""></a></li>
                    <li class="list-inline-item"><a href="<?= base_url(); ?>/assets/javascript:void(0)"><img src="<?= base_url(); ?>images/payments/paypal.png" class="avatar avatar-ex-sm" title="Paypal" alt=""></a></li>
                    <li class="list-inline-item"><a href="<?= base_url(); ?>/assets/javascript:void(0)"><img src="<?= base_url(); ?>images/payments/visa.png" class="avatar avatar-ex-sm" title="Visa" alt=""></a></li>
                </ul>
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</footer>
<!--end footer-->
<!-- Footer End -->