<?php

class Testing extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Singapore');
    }

    public function index()
    {
        // echo '1 jam kedepan: ' . date('H:i:s', time() - (60 * 120)) . '<br/>';
        $this->load->library('whatsapp');
        // $cek_nomor = json_decode($this->whatsapp->checkNumber('082347996363'), true);
        // if ($cek_nomor["status"] === true) {
        //     echo "terdaftar";
        // } else {
        //     echo "tidak terdaftar";
        // }
        // echo "tes";
        // $dari = "2021-03-25"; // tanggal mulai
        // $sampai = "2021-04-02"; // tanggal akhir

        // while (strtotime($dari) <= strtotime($sampai)) {
        //     echo "$dari<br/>";
        //     $dari = date("Y-m-d", strtotime("+1 day", strtotime($dari))); //looping tambah 1 date
        // }
        $pesan = nl2br("Hello \n World");
        $kirim_pesan = $this->whatsapp->sendMessage("082347996363", $pesan);
        // $kirim_pesan = $this->whatsapp->checkNumber("082347996363");
        print_r(json_decode($kirim_pesan));
    }
}
