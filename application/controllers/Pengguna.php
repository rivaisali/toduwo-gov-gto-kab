<?php
class Pengguna extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		$this->load->model('users_model');
		if (!$this->session->userdata('user') && user("level") != "admin") {
			redirect('/logout');
		}
	}

	private $base = 'pengguna';
	private $folder = 'pengguna';

	public function index()
	{
		$data['title']			=	"Pengguna";
		$data['page']			=	$this->folder . "/index";
		$data["data"]			=	$this->crud_model->select_all_where("user", "level", "user");
		$data['base']			=	$this->base;
		$this->load->view("backend/main", $data);
	}

	public function tambah()
	{
		if ($this->form_validation->run("pengguna") == FALSE) {
			$data['title']           =  "Tambah Pengguna";
			$data['page']            =  $this->folder . "/tambah";
			$data['base']            =  $this->base;
			$this->load->view("backend/main", $data);
		} else {
			$data    =    [
				"user_id"       =>  $this->crud_model->cek_id("user", "user_id"),
				"nama"			=>  $this->input->post("nama", true),
				"email"         =>  $this->input->post("email", true),
				"no_telp"       =>  $this->input->post("no_telp", true),
				"password"		=>  password_hash($this->input->post("password", true), PASSWORD_DEFAULT),
				"status"		=>	"1",
				"level"			=>	"user"
			];
			$simpan = $this->crud_model->insert("user", $data);
			if ($simpan) {
				$notifikasi        =    array(
					"status"    =>    "success", "msg"    =>    "User berhasil ditambah"
				);
			} else {
				$notifikasi        =    array(
					"status"    =>    "danger", "msg"    =>    "User gagal ditambah"
				);
			}
			$this->session->set_flashdata("notifikasi", $notifikasi);
			redirect($this->base);
		}
	}

	// ubah
	public function ubah($id = null)
	{
		if ($id === null) {
			redirect($this->base);
		} else {
			$cek_data    =    $this->crud_model->cek_data_where_array("user", ["user_id" => $id]);
			if ($cek_data) {
				redirect($this->base);
			} else {
				if ($this->form_validation->run("pengguna_ubah") == FALSE) {
					$data['data']            =  $this->crud_model->select_one("user", "user_id", $id);
					$data['title']           =  "Ubah Pengguna";
					$data['page']            =  $this->folder . "/ubah";
					$data['base']            =  $this->base;
					$this->load->view("backend/main", $data);
				} else {
					$data    =    [
						"nama"			=>  $this->input->post("nama", true),
						"email"         =>  $this->input->post("email", true),
						"no_telp"       =>  $this->input->post("no_telp", true),
					];
					if ($this->input->post('password', true) != "") {
						$data["password"]	=	password_hash($this->input->post('password', true), PASSWORD_DEFAULT);
					}
					$simpan = $this->crud_model->update("user", $data, "user_id", $this->input->post("id"));
					if ($simpan) {
						$notifikasi        =    array(
							"status"    =>    "success", "msg"    =>    "Pengguna berhasil diubah"
						);
					} else {
						$notifikasi        =    array(
							"status"    =>    "danger", "msg"    =>    "Pengguna gagal diubah"
						);
					}
					$this->session->set_flashdata("notifikasi", $notifikasi);
					redirect($this->base);
				}
			}
		}
	}

	// reset
	public function reset($id = null)
	{
		if ($id === null) {
			redirect($this->base);
		} else {
			$cek_data    =    $this->crud_model->cek_data_where_array("user", ["user_id" => $id]);
			if ($cek_data) {
				redirect($this->base);
			} else {
				$data    =    [
					"password"			=>  password_hash("12345678", PASSWORD_DEFAULT)
				];
				$simpan = $this->crud_model->update("user", $data, "user_id", $id);
				if ($simpan) {
					$notifikasi        =    array(
						"status"    =>    "success", "msg"    =>    "Password Pengguna berhasil direset"
					);
				} else {
					$notifikasi        =    array(
						"status"    =>    "danger", "msg"    =>    "Password Pengguna gagal direset"
					);
				}
				$this->session->set_flashdata("notifikasi", $notifikasi);
				redirect($this->base);
			}
		}
	}

	// hapus
	public function hapus($id = null)
	{
		if ($id === null) {
			redirect($this->base);
		} else {
			$cek_data    =    $this->crud_model->cek_data_where_array("user", ["user_id" => $id]);
			if ($cek_data) {
				redirect($this->base);
			} else {
				$hapus = $this->crud_model->hapus_id("user", "user_id", $id);
				if ($hapus) {
					$notifikasi        =    array(
						"status"    =>    "success", "msg"    =>    "Pengguna berhasil dihapus"
					);
				} else {
					$notifikasi        =    array(
						"status"    =>    "danger", "msg"    =>    "Pengguna gagal dihapus"
					);
				}
				$this->session->set_flashdata("notifikasi", $notifikasi);
				redirect($this->base);
			}
		}
	}

	// toggle status
	public function toggle($id = null)
	{
		if ($id === null) {
			redirect($this->base);
		} else {
			$cek_data    =    $this->crud_model->select_one("user", "user_id", $id);
			if (empty($cek_data)) {
				redirect($this->base);
			} else {
				if ($cek_data->status == "1") {
					$data["status"]	=	"0";
				} else {
					$data["status"]	=	"1";
				}
				$ubah = $this->crud_model->update("user", $data, "user_id", $id);
				if ($ubah) {
					$notifikasi        =    array(
						"status"    =>    "success", "msg"    =>    "Status Pengguna berhasil diubah"
					);
				} else {
					$notifikasi        =    array(
						"status"    =>    "danger", "msg"    =>    "Status Pengguna gagal diubah"
					);
				}
				$this->session->set_flashdata("notifikasi", $notifikasi);
				redirect($this->base);
			}
		}
	}

	// cek email
	function check_email($post_string)
	{
		$cek_data	=	$this->crud_model->select_one_where_array("user", [
			"user_id <>"	=>	$this->input->post('id', true),
			"email"			=>	$post_string
		]);
		if (empty($cek_data)) {
			return TRUE;
		} else {
			$this->form_validation->set_message('check_email', 'Email telah digunakan');
			return FALSE;
		}
		//return $post_string == '0' ? FALSE : TRUE;
	}
}
