<?php

use PhpOffice\PhpSpreadsheet\IOFactory;

class Undangan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Singapore');
        require_once APPPATH . '/third_party/phpspreadsheet/autoload.php';

        $this->load->model('users_model');
        if (!$this->session->userdata('user')) {
            $this->load->helper('url');
            $this->session->set_userdata('last_page', current_url());
            redirect('/login');
        }
    }

    private $base = 'undangan';
    private $folder = 'undangan';

    public function index()
    {
        $data['title'] = "Undangan";
        $data['page'] = $this->folder . "/index";
        // $data['data'] = $this->crud_model->select_custom("select * FROM undangan WHERE (tanggal > '" . date("Y-m-d") . "' or (tanggal = '" . date("Y-m-d") . "' and jam > '" . date('H:i:s', time() - (60 * 120)) . "')) and user_id = '" . user("user_id") . "'");
        $data['data'] = $this->crud_model->select_custom("select * FROM undangan WHERE (tanggal_selesai > '" . date("Y-m-d") . "' or (tanggal_selesai = '" . date("Y-m-d") . "' and jam_selesai > '" . date('H:i:s') . "')) and user_id = '" . user("user_id") . "'");
        $data['base'] = $this->base;
        $this->load->view("backend/main", $data);
    }

    // detail
    public function detail($id = null)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("undangan", ["undangan_id" => $id, "user_id" => user("user_id")]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                $this->load->model("join_model");
                $undangan = $this->crud_model->select_one("undangan", "undangan_id", $id);
                $data['title'] = "Detail Undangan <b>" . $undangan->judul . "</b>";
                $data['page'] = $this->folder . "/detail";
                $data["data"] = $undangan;
                $data['tamu'] = $this->join_model->dua_tabel_where("tamu_undangan", "tamu", "tamu_id", "undangan_id", $id);
                $data['tamu_belum_lihat_undangan'] = $this->crud_model->select_all_where_array_num_row("tamu_undangan", ["undangan_id" => $id, "lihat_undangan" => "0"]);
                $data['base'] = $this->base;
                $this->load->view("backend/main", $data);
            }
        }
    }

    public function tambah()
    {
        if ($this->form_validation->run("undangan") == false) {
            $data['title'] = "Tambah Undangan";
            $data['page'] = $this->folder . "/tambah";
            $data['base'] = $this->base;
            $this->load->view("backend/main", $data);
        } else {
            $data = [
                "undangan_id" => generateRandomString(10),
                "judul" => $this->input->post("judul", true),
                "keterangan" => $this->input->post("keterangan", true),
                "tanggal" => substr($this->input->post("waktu", true), 0, 10),
                "jam" => substr($this->input->post("waktu", true), 11, 5),
                "nama_lokasi" => $this->input->post("nama_lokasi", true),
                "alamat_lokasi" => $this->input->post("alamat_lokasi", true),
                "latlng_lokasi" => $this->input->post("latlng_lokasi", true),
                "user_id" => user("user_id"),
            ];

            $waktu_selesai = $this->input->post('waktu_selesai', true);
            if ($waktu_selesai != "") {
                $data["tanggal_selesai"] = substr($waktu_selesai, 0, 10);
                $data["jam_selesai"] = substr($waktu_selesai, 11, 5);
            }

            $config['upload_path'] = './uploads/undangan/';
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = 10000;
            $config['max_filename'] = '255';
            $config['encrypt_name'] = true;

            $this->load->library('upload', $config);
            if ($this->upload->do_upload('file_undangan')) {
                $data_file = $this->upload->data();
                $data['file_undangan'] = $data_file['file_name'];
            }

            $simpan = $this->crud_model->insert("undangan", $data);
            if ($simpan) {
                $notifikasi = array(
                    "status" => "success", "msg" => "Undangan berhasil ditambah",
                );
            } else {
                $notifikasi = array(
                    "status" => "danger", "msg" => "Undangan gagal ditambah",
                );
            }
            $this->session->set_flashdata("notifikasi", $notifikasi);
            redirect($this->base);
        }
    }

    // ubah
    public function ubah($id = null)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("undangan", ["undangan_id" => $id, "user_id" => user("user_id")]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                $undangan = $this->crud_model->select_one("undangan", "undangan_id", $id);
                if ($this->form_validation->run("undangan") == false) {
                    $data['data'] = $this->crud_model->select_one("undangan", "undangan_id", $id);
                    $data['title'] = "Ubah Undangan";
                    $data['page'] = $this->folder . "/ubah";
                    $data['base'] = $this->base;
                    $this->load->view("backend/main", $data);
                } else {
                    $data = [
                        "judul" => $this->input->post("judul", true),
                        "keterangan" => $this->input->post("keterangan", true),
                        "tanggal" => substr($this->input->post("waktu", true), 0, 10),
                        "jam" => substr($this->input->post("waktu", true), 11, 5),
                        "nama_lokasi" => $this->input->post("nama_lokasi", true),
                        "alamat_lokasi" => $this->input->post("alamat_lokasi", true),
                        "latlng_lokasi" => $this->input->post("latlng_lokasi", true),
                    ];

                    $config['upload_path'] = './uploads/undangan/';
                    $config['allowed_types'] = 'pdf';
                    $config['max_size'] = 10000;
                    $config['max_filename'] = '255';
                    $config['encrypt_name'] = true;

                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload('file_undangan')) {
                        unlink('./uploads/undangan/' . $undangan->file_undangan);
                        $data_file = $this->upload->data();
                        $data['file_undangan'] = $data_file['file_name'];
                    }

                    $simpan = $this->crud_model->update("undangan", $data, "undangan_id", $this->input->post("id"));
                    if ($simpan) {
                        $notifikasi = array(
                            "status" => "success", "msg" => "Undangan berhasil diubah",
                        );
                    } else {
                        $notifikasi = array(
                            "status" => "danger", "msg" => "Undangan gagal diubah",
                        );
                    }
                    $this->session->set_flashdata("notifikasi", $notifikasi);
                    redirect($this->base);
                }
            }
        }
    }

    // hapus
    public function hapus($id = null)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data = $this->crud_model->select_one_where_array("undangan", ["undangan_id" => $id, "user_id" => user("user_id")]);
            if (empty($cek_data)) {
                redirect($this->base);
            } else {
                $hapus = $this->crud_model->hapus_id("undangan", "undangan_id", $id);
                if ($hapus) {
                    unlink('./uploads/undangan/' . $cek_data->file_undangan);
                    $notifikasi = array(
                        "status" => "success", "msg" => "Undangan berhasil dihapus",
                    );
                } else {
                    $notifikasi = array(
                        "status" => "danger", "msg" => "Undangan gagal diubah",
                    );
                }
                $this->session->set_flashdata("notifikasi", $notifikasi);
                redirect($this->base);
            }
        }
    }

    // tambah tamu
    public function tambah_tamu($id = null)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("undangan", ["undangan_id" => $id, "user_id" => user("user_id")]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                if ($this->form_validation->run("tamu_undangan_banyak") == false) {
                    $this->load->model("join_model");
                    $user_id = user("user_id");
                    $query = "SELECT * FROM tamu WHERE user_id = ' $user_id ' and tamu_id NOT IN(SELECT tamu_id  FROM tamu_undangan where undangan_id = '$id')";
                    $undangan = $this->crud_model->select_one("undangan", "undangan_id", $id);
                    $data['title'] = "Tambah Tamu. Untuk Kegiatan <b>" . $undangan->judul . "</b>";
                    $data['page'] = $this->folder . "/tambah_tamu";
                    $data["undangan"] = $undangan;
                    $data["data"] = $this->crud_model->select_custom($query);
                    $data['tamu'] = $this->join_model->dua_tabel_where_array("tamu_undangan", "tamu", "tamu_id", ["undangan_id" => $id]);
                    $data['base'] = $this->base;
                    $this->load->view("backend/main", $data);
                } else {
                    $tamu = $this->input->post('tamu', true);
                    $ind = 0;
                    foreach ($tamu as $t) :
                        $tamu_undangan_id = generateRandomString(10);
                        $data[$ind++] = [
                            "tamu_undangan_id" => $tamu_undangan_id,
                            "undangan_id" => $this->input->post('undangan_id', true),
                            "tamu_id" => $t,
                            "status" => "0",
                        ];

                        $this->load->library("barcode");
                        $this->barcode->createQrCode($tamu_undangan_id);
                    endforeach;

                    $simpan = $this->crud_model->insert_batch("tamu_undangan", $data);
                    if ($simpan) {
                        $notifikasi = array(
                            "status" => "success", "msg" => "Tamu berhasil disimpan",
                        );
                    } else {
                        $notifikasi = array(
                            "status" => "danger", "msg" => "Tamu gagal disimpan",
                        );
                    }
                    $this->session->set_flashdata("notifikasi", $notifikasi);
                    redirect($this->base . "/detail/" . $this->input->post('undangan_id', true));
                }
            }
        }
    }

    // tambah tamu grupt
    public function tambah_tamu_grup($id = null)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("undangan", ["undangan_id" => $id, "user_id" => user("user_id")]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                if ($this->form_validation->run("tamu_undangan_grup") == false) {
                    $this->load->model("join_model");
                    $undangan = $this->crud_model->select_one("undangan", "undangan_id", $id);
                    $data['title'] = "Tambah Tamu Untuk Kegiatan <b>" . $undangan->judul . "</b>";
                    $data['page'] = $this->folder . "/tambah_tamu_grup";
                    $data["undangan"] = $undangan;
                    $data["data"] = $this->crud_model->select_all_where("grup_tamu", "user_id", user("user_id"));
                    $data['base'] = $this->base;
                    $this->load->view("backend/main", $data);
                } else {
                    $tamu = $this->input->post('tamu', true);
                    $ind = 0;
                    $data = [];
                    foreach ($tamu as $t) :
                        $undangan_id = $this->input->post('undangan_id', true);
                        $data_tamu = $this->crud_model->select_all_where("grup_tamu_detail", "grup_tamu_id", $t);
                        foreach ($data_tamu as $dt) {
                            $cek = $this->crud_model->cek_data_where_array("tamu_undangan", [
                                "undangan_id" => $undangan_id,
                                "tamu_id" => $dt->tamu_id,
                            ]);
                            if ($cek) {
                                $cek_wa = $this->crud_model->cek_data_where_array("tamu", [
                                    "tamu_id" => $dt->tamu_id,
                                    "whatsapp" => "1",
                                ]);
                                if (!$cek_wa) {
                                    $tamu_undangan_id = generateRandomString(10);
                                    $data[$ind++] = [
                                        "tamu_undangan_id" => $tamu_undangan_id,
                                        "undangan_id" => $undangan_id,
                                        "tamu_id" => $dt->tamu_id,
                                        "status" => "0",
                                    ];

                                    $this->load->library("barcode");
                                    $this->barcode->createQrCode($tamu_undangan_id);
                                }
                            }
                        }
                    endforeach;

                    if (empty($data)) {
                        $notifikasi = array(
                            "status" => "success", "msg" => "Tamu berhasil disimpan",
                        );
                    } else {
                        $simpan = $this->crud_model->insert_batch("tamu_undangan", $data);
                        if ($simpan) {
                            $notifikasi = array(
                                "status" => "success", "msg" => "Tamu berhasil disimpan",
                            );
                        } else {
                            $notifikasi = array(
                                "status" => "danger", "msg" => "Tamu gagal disimpan",
                            );
                        }
                    }
                    $this->session->set_flashdata("notifikasi", $notifikasi);
                    redirect($this->base . "/detail/" . $this->input->post('undangan_id', true));
                }
            }
        }
    }

    // ambil tamu di master
    public function get_autocomplete($key = null, $id = null)
    {
        if ($key === null && $id === null) {
            echo json_encode([]);
        } else {
            // if ($this->input->post('search', true)) {
            $query = "select tamu_id, nama_lengkap, alamat, no_telp, email, instansi, jabatan from tamu where no_telp like '%$key%' and tamu_id NOT IN (SELECT tamu_id from tamu_undangan WHERE undangan_id = '$id')";
            $result = $this->crud_model->select_custom($query);
            // $result = $this->crud_model->select_like("tamu", "no_telp", $key);
            if (count($result) > 0) {
                foreach ($result as $row) {
                    $arr_result[] = array(
                        'value' => $row->no_telp,
                        'nama' => $row->nama_lengkap,
                        'no_telp' => $row->no_telp,
                        'alamat' => $row->alamat,
                        'email' => $row->email,
                        'instansi' => $row->instansi,
                        'jabatan' => $row->jabatan,
                    );
                }

                echo json_encode($arr_result);
            }
        }
    }

    // tambah tamu lain
    public function tambah_tamu_lain($id)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("undangan", ["undangan_id" => $id, "user_id" => user("user_id")]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                if ($this->form_validation->run("tamu") == false) {
                    $undangan = $this->crud_model->select_one("undangan", "undangan_id", $id);
                    $data['title'] = "Tambah Tamu. Untuk Kegiatan <b>" . $undangan->judul . "</b>";
                    $data['page'] = $this->folder . "/tambah_tamu_lain";
                    $data['undangan'] = $undangan;
                    $data['base'] = $this->base . "/tambah_tamu/" . $id;
                    $this->load->view("backend/main", $data);
                } else {
                    $data = [
                        "tamu_id" => $this->crud_model->cek_id("tamu", "tamu_id"),
                        "nama_lengkap" => $this->input->post("nama", true),
                        "alamat" => $this->input->post("alamat", true),
                        "email" => $this->input->post("email", true),
                        "no_telp" => $this->input->post("no_telp", true),
                        "instansi" => $this->input->post("instansi", true),
                        "jabatan" => $this->input->post("jabatan", true),
                        "user_id" => user("user_id"),
                    ];
                    $simpan = $this->crud_model->insert("tamu", $data);
                    if ($simpan) {
                        $data_tamu_undangan = [
                            "tamu_undangan_id" => generateRandomString(10),
                            "undangan_id" => $id,
                            "tamu_id" => $data["tamu_id"],
                            "status" => "0",
                        ];
                        $this->crud_model->insert("tamu_undangan", $data_tamu_undangan);
                        $this->load->library("barcode");
                        $this->barcode->createQrCode($data_tamu_undangan["tamu_undangan_id"]);
                        $notifikasi = array(
                            "status" => "success", "msg" => "Tamu berhasil ditambah",
                        );
                    } else {
                        $notifikasi = array(
                            "status" => "danger", "msg" => "Tamu gagal ditambah",
                        );
                    }
                    $this->session->set_flashdata("notifikasi", $notifikasi);
                    redirect($this->base . "/detail/" . $id);
                }
            }
        }
    }

    // hapus tamu
    public function hapus_tamu($id = null)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("tamu_undangan", ["tamu_undangan_id" => $id]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                $tamu = $this->crud_model->select_one("tamu_undangan", "tamu_undangan_id", $id);
                $hapus = $this->crud_model->hapus_id("tamu_undangan", "tamu_undangan_id", $id);
                if ($hapus) {
                    unlink('./uploads/qrcode/' . $id . '.png');
                    $notifikasi = array(
                        "status" => "success", "msg" => "Undangan berhasil dihapus",
                    );
                } else {
                    $notifikasi = array(
                        "status" => "danger", "msg" => "Undangan gagal diubah",
                    );
                }
                $this->session->set_flashdata("notifikasi", $notifikasi);
                redirect($this->base . "/detail/" . $tamu->undangan_id);
            }
        }
    }

    // cek option bernilai 0
    public function check_default($post_string)
    {
        if ($post_string == '0') {
            $this->form_validation->set_message('check_default', '{field} Belum dipilih');
            return false;
        } else {
            return true;
        }
        //return $post_string == '0' ? FALSE : TRUE;
    }

    // cek tanggal acara
    public function tgl_check($post_string)
    {
        $now = date("Y-m-d H:i");
        if ($post_string < $now) {
            $this->form_validation->set_message('tgl_check', 'Waktu tidak valid');
            return false;
        } else {
            if ($this->input->post('waktu_selesai', true) != "") {
                if ($post_string > $this->input->post('waktu_selesai', true)) {
                    $this->form_validation->set_message('tgl_check', 'Waktu tidak valid');
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }
    }

    // fungsi cek tamu
    public function tamu_check()
    {
        if (!$this->input->post('tamu[]')) {
            $this->form_validation->set_message('tamu_check', 'Tamu belum dipilih');
            return false;
        } else {
            return true;
        }
    }

    // cek file
    public function check_file($str, $name)
    {
        if ($this->input->post('id')) { // jika edit
            $allowed_mime_type_arr = array('application/pdf');
            if (isset($_FILES[$name]['name']) && $_FILES[$name]['name'] != "") {
                $mime = get_mime_by_extension($_FILES[$name]['name']);
                if (in_array($mime, $allowed_mime_type_arr)) {
                    if ($_FILES[$name]['size'] > '10485760') {
                        $this->form_validation->set_message('check_file', 'Maksimal 10MB');
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    $this->form_validation->set_message('check_file', 'File undangan harus PDF');
                    return false;
                }
            } else {
                return true;
            }
        } else { // jika tambah
            $allowed_mime_type_arr = array('application/pdf');
            if (isset($_FILES[$name]['name']) && $_FILES[$name]['name'] != "") {
                $mime = get_mime_by_extension($_FILES[$name]['name']);
                if (in_array($mime, $allowed_mime_type_arr)) {
                    if ($_FILES[$name]['size'] > '10485760') {
                        $this->form_validation->set_message('check_file', 'Maksimal 10MB');
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    $this->form_validation->set_message('check_file', 'File undangan harus PDF');
                    return false;
                }
            } else {
                $this->form_validation->set_message('check_file', 'File Undangan belum dipilih');
                return false;
            }
        }
    }

    // cek nomor hp terdaftar di WA
    public function check_nomor($post_string)
    {
        if ($post_string == "") {
            $this->form_validation->set_message('check_nomor', '{field} Harus diisi');
            return false;
        } else {
            $this->load->library('whatsapp');
            $nomor = json_decode($this->whatsapp->checkNumber($post_string), true);
            if ($nomor["status"] === true) {
                return true;
            } else {
                $this->form_validation->set_message('check_nomor', 'Nomor Tidak terdaftar DI WA');
                return false;
            }
        }
    }

    // riwayat undangan
    public function riwayat_undangan()
    {
        $data['title'] = "Riwayat Undangan";
        $data['page'] = $this->folder . "/riwayat";
        // $data['data'] = $this->crud_model->select_custom("select * FROM undangan WHERE (tanggal < '" . date("Y-m-d") . "' or (tanggal = '" . date("Y-m-d") . "' and jam < '" . date("H:i:s") . "')) and user_id = '" . user("user_id") . "'");
        $data['data'] = $this->crud_model->select_custom("select * FROM undangan WHERE (tanggal_selesai < '" . date("Y-m-d") . "' or (tanggal_selesai = '" . date("Y-m-d") . "' and jam_selesai < '" . date("H:i:s") . "')) and user_id = '" . user("user_id") . "'");
        $data['base'] = $this->base;
        $this->load->view("backend/main", $data);
    }

    // detail riwayat
    public function detail_riwayat($id = null)
    {
        if ($id === null) {
            redirect("riwayat_undangan");
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("undangan", ["undangan_id" => $id, "user_id" => user("user_id")]);
            if ($cek_data) {
                redirect("riwayat_undangan");
            } else {
                $this->load->model("join_model");
                $undangan = $this->crud_model->select_one("undangan", "undangan_id", $id);
                $data['title'] = "Detail Undangan <b>" . $undangan->judul . "</b>";
                $data['page'] = $this->folder . "/detail_riwayat";
                $data["data"] = $undangan;
                $data['tamu'] = $this->join_model->dua_tabel_where("tamu_undangan", "tamu", "tamu_id", "undangan_id", $id);
                $data['base'] = "riwayat_undangan";
                $this->load->view("backend/main", $data);
            }
        }
    }

    // ambil data absen
    public function absen($id = null)
    {
        if ($id === null) {
            redirect("riwayat_undangan");
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("undangan", ["undangan_id" => $id, "user_id" => user("user_id")]);
            if ($cek_data) {
                redirect("riwayat_undangan");
            } else {
                $this->load->model("join_model");
                $undangan = $this->crud_model->select_one("undangan", "undangan_id", $id);
                $data['title'] = "Daftar Hadir Undangan <b>" . $undangan->judul . "</b>";
                $data['page'] = $this->folder . "/detail_absen";
                $data["data"] = $undangan;

                $filter = $this->input->post('kategori', true);
                $tanggal = $this->input->post('tanggal', true);
                if (empty($tanggal) || $tanggal == "all") {
                    $data['tanggal'] = "all";
                } else {
                    $data['tanggal'] = $tanggal;
                }

                if (empty($filter) || $filter == "all") {
                    $data['filter'] = "all";
                    $data['tamu'] = $this->join_model->dua_tabel_where("tamu_undangan", "tamu", "tamu_id", "undangan_id", $id);
                } else {
                    if ($filter == "2") {
                        $filter = "0";
                    }
                    $data['filter'] = $filter;
                    $data['tamu'] = $this->join_model->dua_tabel_where_array("tamu_undangan", "tamu", "tamu_id", ["undangan_id" => $id, "status" => $filter]);
                };

                $data['base'] = "riwayat_undangan";
                $this->load->view("backend/main", $data);
            }
        }
    }

    public function cetak_absen($undangan_id = null, $kategori = null, $tanggal = null)
    {
        $where = [];
        $where = ["undangan_id" => $undangan_id];
        if ($kategori === null || $kategori == "all") {
            $data['kategori'] = "all";
            $where["tamu_undangan.status <>"] = "2";
        } else {
            $where["tamu_undangan.status"] = $kategori;
            $data['kategori'] = $kategori;
        }

        if ($tanggal === null) {
            $data["tanggal"] = "all";
        } else {
            $data["tanggal"] = $tanggal;
        }

        $data['undangan'] = $this->crud_model->select_one("undangan", "undangan_id", $undangan_id);
        $this->load->model("join_model");
        $data['tamu'] = $this->join_model->dua_tabel_where_array("tamu_undangan", "tamu", "tamu_id", $where);
        $this->load->library("barcode");
        $this->barcode->createQrCodeUndangan(base_url("undangan/" . $undangan_id), $undangan_id);
        $this->load->view("backend/undangan/cetak_absen", $data);
    }

    public function export_xls($undangan_id = null, $kategori = null, $tanggal = null)
    {
        $where = [];
        $where = ["undangan_id" => $undangan_id];
        if ($kategori === null || $kategori == "all") {
            $kategori = "Semua";
            $where["tamu_undangan.status <>"] = "2";
        } else {
            if ($kategori == "1") {
                $kategori = "Hadir";
            } else {
                $kategori = "Tidak Hadir";
            }
            $where["tamu_undangan.status"] = $kategori;
        }

        if ($tanggal === null || $tanggal == "all") {
            $tanggal_check_in = "Semua";
        } else {
            $tanggal_check_in = tgl_indonesia($tanggal);
        }

        $undangan = $this->crud_model->select_one("undangan", "undangan_id", $undangan_id);
        $this->load->model("join_model");
        $tamu = $this->join_model->dua_tabel_where_arrays("tamu_undangan", "tamu", "tamu_id", $where);
        $this->load->library("barcode");
        $this->barcode->createQrCodeUndangan(base_url("undangan/" . $undangan_id), $undangan_id);

        $reader = IOFactory::createReader('Xls');
        $spreadsheet = $reader->load(APPPATH . '/third_party/template_absen1.xls');
        if ($undangan->tanggal == $undangan->tanggal_selesai) {
            $waktu = tgl_full($undangan->tanggal . " " . $undangan->jam) . " Sampai " . jam($undangan->tanggal_selesai . " " . $undangan->jam_selesai);
        } else {
            $waktu = tgl_full($undangan->tanggal . " " . $undangan->jam) . " Sampai " . tgl_full($undangan->tanggal_selesai . " " . $undangan->jam_selesai);
        }

        // $spreadsheet->getActiveSheet()->getStyle('F20:F999')->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->setCellValue('C11', " : " . $undangan->judul);
        $spreadsheet->getActiveSheet()->setCellValue('C12', " : " . $waktu);
        $spreadsheet->getActiveSheet()->setCellValue('C13', " : " . $undangan->nama_lokasi);
        $spreadsheet->getActiveSheet()->setCellValue('C14', " : " . ambil_nama_by_id("user", "nama", "user_id", $undangan->user_id));
        $spreadsheet->getActiveSheet()->setCellValue('C15', " : " . $kategori);
        $spreadsheet->getActiveSheet()->setCellValue('C16', " : " . $tanggal_check_in);

        $baseRow = 20;
        foreach ($tamu as $r => $dataRow) {
            if ($dataRow["status"] == "1") {
                $ket = "Hadir";
            } else {
                $ket = "Tidak Hadir";
            }

            $where_check_in = [
                "tamu_undangan_id" => $dataRow['tamu_undangan_id'],
            ];
            if ($tanggal != "all") {
                $where_check_in["tanggal_check_in"] = $tanggal;
            }

            if ($tanggal == "all") {
                $cek_check_in = ambil_data_by_id_where_array("check_in", $where_check_in);
            } else {
                $cek_check_in = ambil_data_by_id_row_where_array("check_in", $where_check_in);
            }

            $waktu_check_in = "";
            if (!empty($cek_check_in)) {

                if ($tanggal == "all") {
                    $no_check_in = 1;
                    foreach ($cek_check_in as $ci) :
                        if ($no_check_in > 1) $waktu_check_in .= "\n";
                        $waktu_check_in .= tgl_full($ci->tanggal_check_in . " " . $ci->jam_check_in);
                        $no_check_in++;
                    endforeach;
                } else {
                    $waktu_check_in .= tgl_full($cek_check_in->tanggal_check_in . " " . $cek_check_in->jam_check_in);
                }
            }

            $row = $baseRow + $r;
            $spreadsheet->getActiveSheet()->insertNewRowBefore($row, 1);

            $spreadsheet->getActiveSheet()->setCellValue('A' . $row, $r + 1)
                ->setCellValue('B' . $row, $dataRow['nama_lengkap'])
                ->setCellValue('C' . $row, $dataRow['alamat'])
                ->setCellValue('D' . $row, $dataRow['instansi'])
                ->setCellValue('E' . $row, $dataRow['jabatan'])
                ->setCellValue('F' . $row, $ket)
                ->setCellValue('G' . $row, $waktu_check_in);

            $spreadsheet->getActiveSheet()->getStyle('G' . $row)->getAlignment()->setWrapText(true);
        }

        $spreadsheet->getActiveSheet()->removeRow($baseRow - 1, 1);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Absensi"' . $undangan->judul . '".xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }

    public function kirim_undangan($id)
    {
        if ($id === null) {
            $ret = [
                "status" => "0",
                "message" => "Tamu Tidak ditemukan",
                "error" => true,
            ];
        } else {
            $tamu = $this->crud_model->select_one("tamu_undangan", "tamu_undangan_id", $id);
            if (empty($tamu)) {
                $ret = [
                    "status" => "0",
                    "message" => "Tamu Tidak ditemukan",
                    "error" => true,
                ];
            } else {
                $this->load->model("join_model");
                $tamu = $this->join_model->dua_tabel_where_array_row("tamu", "tamu_undangan", "tamu_id", [
                    "tamu_undangan.tamu_undangan_id" => $id,
                ]);

                if ($tamu->whatsapp == "0") {
                    $ret = [
                        "status" => "0",
                        "message" => "Nomor tidak terdaftar di WA",
                        "error" => true,
                    ];
                } else {

                    $undangan = $this->crud_model->select_one("undangan", "undangan_id", $tamu->undangan_id);

                    $this->load->library("whatsapp");
                    $pesan = "Assalam alaikum Wr.Wb.\n";
                    $pesan .= "Kepada Yth *" . $tamu->nama_lengkap . "* \n";
                    $pesan .= "Dengan ini *" . user("nama") . "* mengundang Bapak/Ibu untuk menghadiri Acara *" . $undangan->judul . "* yang akan dilaksanakan pada :\n";
                    $pesan .= "Hari : *" . hari($undangan->tanggal) . "*\n";
                    $pesan .= "Tanggal : *" . tgl_indonesia($undangan->tanggal) . "*\n";
                    $pesan .= "Jam : *" . jam($undangan->tanggal . " " . $undangan->jam) . "*\n";
                    $pesan .= "Tempat : *" . $undangan->nama_lokasi . "*\n";
                    $pesan .= "Keterangan : *" . $undangan->keterangan . "*\n";
                    $pesan .= "Untuk Konfirmasi Kehadiran dan data pada Daftar Hadir silahkan klik Link dibawah ini :\n";
                    $pesan .= base_url($tamu->tamu_undangan_id) . "\n";
                    $pesan .= "Demikian undangan ini kami sampaikan dan atas perhatiannya kami ucapkan terima kasih.\n";
                    $pesan .= "*TTD*\n";
                    $pesan .= user("nama") . "\n\n";
                    $pesan .= "_Harap Balas *Ya* jika link konfirmasi kehadiran tidak bisa di Klik_\n";
                    $pesan .= "Send By Toduwo.id\n";
                    $this->whatsapp->sendMessage($tamu->no_telp, $pesan);
                    $this->whatsapp->sendDocument($tamu->no_telp, "File Undangan", base_url("uploads/undangan/" . $undangan->file_undangan));

                    $ret = [
                        "status" => "1",
                        "message" => "Undangan Terkirim",
                    ];
                }
            }
        }
        echo json_encode($ret);
    }

    public function kirim_undangan_banyak($id)
    {
        if ($id === null) {
            $ret = [
                "status" => "0",
                "message" => "Undangan Tidak ditemukan",
                "error" => true,
            ];
        } else {
            $undangan = $this->crud_model->select_one("undangan", "undangan_id", $id);
            if (empty($undangan)) {
                $ret = [
                    "status" => "0",
                    "message" => "Undangan Tidak ditemukan",
                    "error" => true,
                ];
            } else {
                $this->load->model("join_model");
                $tamu = $this->join_model->dua_tabel_where_array("tamu", "tamu_undangan", "tamu_id", [
                    "lihat_undangan" => "0",
                    "undangan_id" => $id,
                ]);

                foreach ($tamu as $tamu) :
                    if ($tamu->whatsapp == "1") {
                        $this->load->library("whatsapp");
                        $pesan = "Assalam alaikum Wr.Wb.\n";
                        $pesan .= "Kepada Yth *" . $tamu->nama_lengkap . "* \n";
                        $pesan .= "Dengan ini *" . user("nama") . "* mengundang Bapak/Ibu untuk menghadiri Acara *" . $undangan->judul . "* yang akan dilaksanakan pada :\n";
                        $pesan .= "Hari : *" . hari($undangan->tanggal) . "*\n";
                        $pesan .= "Tanggal : *" . tgl_indonesia($undangan->tanggal) . "*\n";
                        $pesan .= "Jam : *" . jam($undangan->tanggal . " " . $undangan->jam) . "*\n";
                        $pesan .= "Tempat : *" . $undangan->nama_lokasi . "*\n";
                        $pesan .= "Keterangan : *" . $undangan->keterangan . "*\n";
                        $pesan .= "Untuk Konfirmasi Kehadiran dan data pada Daftar Hadir silahkan klik Link dibawah ini :\n";
                        $pesan .= base_url($tamu->tamu_undangan_id) . "\n";
                        $pesan .= "Demikian undangan ini kami sampaikan dan atas perhatiannya kami ucapkan terima kasih.\n";
                        $pesan .= "*TTD*\n";
                        $pesan .= user("nama") . "\n\n";
                        $pesan .= "_Harap Balas *Ya* jika link konfirmasi kehadiran tidak bisa di Klik_\n";
                        $pesan .= "Send By Toduwo.id\n";
                        $this->whatsapp->sendMessage($tamu->no_telp, $pesan);
                        $this->whatsapp->sendDocument($tamu->no_telp, "File Undangan", base_url("uploads/undangan/" . $undangan->file_undangan));
                    }
                endforeach;

                $ret = [
                    "status" => "1",
                    "message" => "Undangan Terkirim",
                ];
            }
        }
        echo json_encode($ret);
    }

    public function cek_nomor()
    {
        $this->load->library('whatsapp');
        echo $this->whatsapp->checkNumber('082347996361');
    }
}
