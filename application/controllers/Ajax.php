<?php
class Ajax extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
	}

	// cek kode transaksi
	public function cek_kode_transaksi()
	{
		$ret = [];
		$kode = $this->input->post("kode_transaksi", true);
		$transaksi = $this->crud_model->select_one("user", "kode_transaksi", $kode);
		if (!empty($transaksi)) {
			$ret = [
				"status" => 1,
				"msg" => "Kode ditemukan",
				"data" => $transaksi
			];
		} else {
			$ret = [
				"status" => 0,
				"msg" => "Kode tidak ditemukan",
				"data" => []
			];
		}
		echo json_encode($ret);
	}

	// cek detail paket
	public function cek_paket()
	{
		$ret = [];
		$paket_id = $this->input->post("paket_id", true);
		$data = $this->crud_model->select_one("paket", "paket_id", $paket_id);
		if (!empty($data)) {
			$ret = [
				"status" => 1,
				"msg" => "Paket ditemukan",
				"data" => $data
			];
		} else {
			$ret = [
				"status" => 0,
				"msg" => "Paket tidak ditemukan",
				"data" => []
			];
		}
		echo json_encode($ret);
	}

	// cek promo
	public function cek_promo()
	{
		$ret = [];
		$kode = $this->input->post("kode", true);
		$paket = $this->input->post("paket", true);
		$data = $this->crud_model->select_one_where_array("promo", ["kode_referal" => $kode, "kuota >" => 0, "paket_id" => $paket]);
		if (!empty($data)) {
			if ($data->persen == "1") {
				$msg	=	"Dapatkan potongan " . $data->potongan . "%";
			} else {
				$msg	=	"Dapatkan potongan " . rupiah($data->potongan);
			}
			$ret = [
				"status" => 1,
				"msg" => $msg,
				"data" => $data
			];
		} else {
			$ret = [
				"status" => 0,
				"msg" => "Promo tidak ditemukan",
				"data" => []
			];
		}
		echo json_encode($ret);
	}
}
