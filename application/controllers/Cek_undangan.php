<?php
class Cek_undangan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Singapore');
    }

    public function index($id = null)
    {
        if ($id === null) {
            $data['title']    =    "Halaman Tidak Ditemukan";
            $this->load->view('e404', $data);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("undangan", ["undangan_id" => $id]);
            if ($cek_data) {
                $data['title']    =    "Halaman Tidak Ditemukan";
                $this->load->view('e404', $data);
            } else {
                $data['undangan']   =   $this->crud_model->select_one("undangan", "undangan_id", $id);
                $this->load->view("backend/undangan/cek_undangan", $data);
            }
        }
    }
}
